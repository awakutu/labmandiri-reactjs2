import React, { Component } from "react";
import DateClass from "./DateClass";
import DateFunction from "./DateFunction";

export default class Clock extends Component {
  constructor() {
    super();

    this.state = {
      dateTime: new Date(),
      dateIsFunction: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState((state) => ({
      dateIsFunction: !state.dateIsFunction,
    }));
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      dateTime: new Date(),
    });
  }

  render() {
    const { handleClick } = this;
    const { dateIsFunction, dateTime } = this.state;

    let dateComponent;
    if (dateIsFunction) {
      dateComponent = <DateFunction />;
    } else {
      dateComponent = <DateClass date={dateTime} />;
    }
    return (
      <div>
        {dateComponent}
        {dateTime.toLocaleTimeString()}
        <br />
        <p>Change date component to</p>
        <button
          onClick={handleClick}
          style={{ padding: "5px 10px", fontSize: "1rem" }}
        >
          {dateIsFunction ? "CLASS" : "FUNCTION"}
        </button>
      </div>
    );
  }
}
