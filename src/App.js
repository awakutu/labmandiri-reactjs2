import React from "react";
import "./App.css";
import Clock from "./components/Clock";

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Clock />
      </header>
    </div>
  );
}
