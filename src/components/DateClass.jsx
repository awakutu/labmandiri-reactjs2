import React, {Component, Fragment} from 'react';

export default class  DateClass extends Component{
    render(){
        const {props} = this;
        return(
            <Fragment>
                <h1>Date Class</h1>
                <p>{props.date.toDateString()}</p>
            </Fragment>
        )
    }
}
